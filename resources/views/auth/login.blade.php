@extends('layouts.app')
@section('content')

@auth
<?php
header("Location: https://local.soya-sari.es/home");
exit;
?>
@endauth

<div class="main-wrapper">
    <div class="account-content">
        
        <div class="container">
            <!-- Account Logo -->

            {{-- message --}}
            {!! Toastr::message() !!}
            <!-- /Account Logo -->
            <div class="account-box">
                <div class="account-wrapper">

                    <div class="account-logo">
                        <a><img src="{{ URL::to('assets/img/logologin.png') }}" alt="Soeng Souy"></a>
                    </div>
                    <!-- Account Form -->
                    <form method="POST" action="{{ route('login') }}" id="formulario" onsubmit="bloquear()">
                        @csrf
                        <div class="form-group">
                            <label>Codigo de Usuario</label>
                            <input required type="number" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Codigo" pattern="[0-9]{4,4}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label>Contraseña</label>
                                </div>
                            </div>
                            <input required type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" pattern="[0-9]{4,8}">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <!-- <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label></label>
                                    </div>
                                    <div class="col-auto">
                                        <a class="text-muted" href="{{ route('forget-password') }}">
                                        ¿Has olvidado la contraseña?
                                        </a>
                                    </div>
                                </div>
                            </div> -->
                        <div class="form-group text-center">
                            <button class="btn btn-primary account-btn" id="boton" type="submit">Iniciar Sesión</button>
                        </div>
                        <!-- <div class="account-footer">
                                <p>¿No tienes una cuenta? <a href="{{ route('register') }}">Regístrate</a></p>
                            </div> -->
                    </form>
                    <!-- /Account Form -->
                </div>
            </div>
        </div>
    </div>
    <script>
        function bloquear() {
            var btn = document.getElementById("boton");
            $("#formulario :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>
</div>
@endsection