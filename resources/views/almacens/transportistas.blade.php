@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Transportistas</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Transportistas</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-transportista')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Transportista</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->

        <!-- /Search Filter -->
        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Telefono</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transportistas as $transportista )
                                <tr>
                                    <td  class="id">{{ $transportista->id }}</td>
                                    <td class="nombre">{{ $transportista->nombre }}</td>
                                    <td class="apellido_paterno">{{ $transportista->apellido_paterno }}</td>
                                    <td class="apellido_materno">{{ $transportista->apellido_materno }}</td>
                                    <td class="telefono">{{ $transportista->telefono }}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($transportista->created_at))}}</td>
                                    <td class="text-right">
                                        @can('editar-transportista')
                                        <a class="btn btn-info btn-sm userUpdate" href="#" data-toggle="modal" data-target="#edit_user"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('borrar-transportista')
                                        @if($transportista->historial=='[]')
                                        <a class="btn btn-danger btn-sm userDelete" href="#" data-toggle="modal" data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                        @endif
                                        @endcan

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Transportista</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('trans/add/save') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Nombre</label>
                                <input required class="form-control" type="text" id="nombre" name="nombre" placeholder="ej: Mario" maxlength="50" minlength="3">
                            </div>
                            <div class="col-sm-6">
                                <label>Apellido Paterno</label>
                                <input required class="form-control" type="text" id="apellido_paterno" name="apellido_paterno" placeholder="ej: Perez" maxlength="50" minlength="3">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Apellido Materno</label>
                                <input  class="form-control" type="text" id="apellido_materno" name="apellido_materno" placeholder="ej: Casas" maxlength="50" minlength="3">
                            </div>
                            <div class="col-sm-6">
                                <label>Telefono</label>
                                <input  class="form-control" type="number" id="telefono" name="telefono" placeholder="ej: 75645899" maxlength="50" minlength="3">
                            </div>
                        </div>

                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->

    <!-- Edit User Modal -->
    <div id="edit_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Transportista</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('updatetrans') }}" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <div class="row">
                            <input type="hidden" name="id" id="e_id" >
                            <div class="col-sm-6">
                                <label>Nombre</label>
                                <input required class="form-control" type="text" id="e_nombre" name="nombre" placeholder="ej: Mario" maxlength="50" minlength="3">
                            </div>
                            <div class="col-sm-6">
                                <label>Apellido Paterno</label>
                                <input required class="form-control" type="text" id="e_apellido_paterno" name="apellido_paterno" placeholder="ej: Perez" maxlength="50" minlength="3">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Apellido Materno</label>
                                <input  class="form-control" type="text" id="e_apellido_materno" name="apellido_materno" placeholder="ej: Casas" maxlength="50" minlength="3">
                            </div>
                            <div class="col-sm-6">
                                <label>Telefono</label>
                                <input  class="form-control" type="number" id="e_telefono" name="telefono" placeholder="ej: 75645899" maxlength="50" minlength="3">
                            </div>
                        </div>

                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Transportista</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('trans/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="id" class="e_id" value="">

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_nombre').val(_this.find('.nombre').text());
        $('#e_apellido_paterno').val(_this.find('.apellido_paterno').text());
        $('#e_apellido_materno').val(_this.find('.apellido_materno').text());
        $('#e_telefono').val(_this.find('.telefono').text());
    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());
    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection