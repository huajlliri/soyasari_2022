@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Departamentos</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Departamentos</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-departamento')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Departamento</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->

        <!-- /Search Filter -->
        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Nombre del Departamento</th>
                                    <th>Usuarios Asignados</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($departamentos as $departamento )
                                <?php
                                $contador = 0;
                                ?>
                                @foreach ($user as $usuario )
                                @if($departamento->id==$usuario->departamento_id)
                                <?php
                                $contador = $contador + 1;
                                ?>
                                @endif
                                @endforeach
                                <tr>
                                    <td hidden class="id">{{ $departamento->id }}</td>
                                    <td class="name">{{ $departamento->nombre }}</td>


                                    <td class="usuarios_asignados">{{ $contador }}</td>


                                    <td class="created_at">{{date('d-m-Y',strtotime($departamento->created_at))}}</td>

                                    <td class="text-right">
                                        @can('editar-departamento')
                                        <a class="btn btn-info btn-sm userUpdate" href="#" data-toggle="modal" data-id="'.$user->id.'" data-target="#edit_user"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('borrar-departamento')
                                        @if($contador==0)
                                        <a class="btn btn-danger btn-sm userDelete" href="#" data-toggle="modal" ata-id="'.$user->id.'" data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                        @endif
                                        @endcan

                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Departamento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('dep/add/save') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf

                        <div class="form-group">
                            <label>Nombre del Departamento</label>
                            <input required class="form-control" type="text" id="name" name="name" placeholder="ej: Recursos Humanos" maxlength="50" minlength="3">
                        </div>


                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->

    <!-- Edit User Modal -->
    <div id="edit_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Departamento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('updatedep') }}" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf

                        <input type="hidden" name="id" id="e_id" value="">
                        <div class="form-group">
                            <label>Nombre del Departamento</label>
                            <input required class="form-control" maxlength="50" minlength="3" type="text" name="name" id="e_name" value="" />
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Departamento</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('dep/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="id" class="e_id" value="">

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_name').val(_this.find('.name').text());
        $('#e_id').val(_this.find('.id').text());
    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());

    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection