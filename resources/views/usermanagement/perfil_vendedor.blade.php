@extends('layouts.master')
{{--
@section('menu')
@extends('sidebar.dashboard')
@endsection --}}
@section('content')


<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Perfil de Usuarios</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="{{'/vendedores' }}">vendedores</a></li>
                        <li class="breadcrumb-item active">{{$user->nombre}}</li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- message --}}
        {!! Toastr::message() !!}
        <!-- /Page Header -->
        <div class="card mb-0">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-view">
                            <div class="profile-img-wrap">
                                <div class="profile-img">
                                    <a href="#"><img alt="" src="{{ URL::to('/assets/images/'. $user->avatar) }}" alt="{{ $user->nombre }}"></a>
                                </div>
                            </div>
                            <div class="profile-basic">
                                <div class="row">
                                    <h3 class="user-name m-t-0 mb-0"><span class="text-danger">Nombre: </span>{{ $user->nombre.' '.$user->apellido_paterno.' '.$user->apellido_materno}}</h3>
                                </div><br>
                                <div class="row">
                                    <h4 class="user-name m-t-0 mb-0"><span class="text-danger">Código de Empleado: </span>{{ $user->email}}</h4>
                                </div><br>
                                <div class="row">
                                    <h4 class="user-name m-t-0 mb-0"><span class="text-danger">Departamento: </span>{{ $user->departamento->nombre}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card tab-box">
            <div class="row user-tabs">
                <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="nav-item"><a href="#emp_profile" data-toggle="tab" class="nav-link active">Perfil</a></li>
                        @if($user->departamento->nombre=='Ventas')
                        <li class="nav-item"><a href="#emp_projects" data-toggle="tab" class="nav-link">Clientes</a></li>
                        <li class="nav-item"><a href="#bank_statutory" data-toggle="tab" class="nav-link">Cuadernos</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <!-- Profile Info Tab -->
            <div id="emp_profile" class="pro-overview tab-pane fade show active">
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Informacion Personal</h3>
                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Nombres: </div>
                                        <div class="text-purple">{{$user->nombre}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Apellido Paterno:</div>
                                        <div class="text-purple">{{$user->apellido_paterno}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Apellido Materno:</div>
                                        @if($user->materno=='')
                                        <div class="text-purple"><br> </div>
                                        @else
                                        <div class="text-purple">{{$user->apellido_materno}}</div>
                                        @endif
                                    </li>
                                    <li>
                                        <div class="title">Carnet:</div>
                                        <div class="text-purple">{{$user->carnet}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Teléfono:</div>
                                        <div class="text-purple">{{$user->telefono}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Dirección</div>
                                        <div class="text-purple">{{$user->direccion}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Información Laboral</h3>

                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Código de Empleado</div>
                                        <div class="text-info">{{$user->email}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Departamento</div>
                                        <div class="text-info">{{$user->departamento->nombre}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Rol</div>
                                        <div class="text-info">{{$user->roles->first()->name}}</div>
                                    </li>

                                    <li>
                                        <div class="title">Estado</div>
                                        <div class="text-info">{{$user->estado}}</div>
                                    </li>

                                    <li>
                                        <div class="title">Creado</div>
                                        <div class="text-info">{{$user->created_at->diffForHumans()}}</div>
                                    </li>

                                    <li>
                                        <div class="title">Salario</div>
                                        <div class="text-info">4500</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /Profile Info Tab -->

            <!-- Projects Tab -->
            <div class="tab-pane fade" id="emp_projects">
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Clientes</h3>
                                <table class="table table-striped custom-table datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th hidden>id</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Telefono</th>
                                            <th>Zona</th>
                                            <th class="text-right">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($user->cliente as $cliente )
                                        <tr>
                                            <td hidden class="id">{{ $cliente->id }}</td>
                                            <td>{{ $cliente->nombre }}</td>
                                            <td>{{ $cliente->apellido_paterno }}</td>
                                            <td>{{ $cliente->telefono }}</td>
                                            <td>{{ $cliente->zona }}</td>
                                            <td class="text-right">
                                                @can('ver-perfil-cliente')
                                                <a class="btn btn-success btn-sm" href="{{ url('clientes/perfil/'.$cliente->id) }}"><i class="fa fa-eye"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Deudas Pendientes</h3>
                                <table class="table table-striped custom-table datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th hidden>id</th>
                                            <th>Cuaderno</th>
                                            <th>Cliente</th>
                                            <th>Saldo</th>
                                            <th class="text-right">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cuentas as $cuenta)
                                        @if($cuenta->historial_cuaderno->cuaderno->user_id==$user->id)
                                        <tr>
                                            <td hidden class="id">{{ $cuenta->id }}</td>
                                            <td>{{ $cuenta->historial_cuaderno->cuaderno->codigo }}</td>
                                            <td>{{ $cuenta->historial_cuaderno->cliente->nombre.' '.$cuenta->historial_cuaderno->cliente->apellido_paterno }}</td>
                                            <td>{{ $cuenta->saldo }}</td>
                                            <td class="text-right">
                                                @can('ver-cuaderno')
                                                <a class="btn btn-success btn-sm" href="{{ url('cuadernos/registrar/'.$cuenta->id) }}"><i class="fa fa-eye"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endif

                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Projects Tab -->

            <!-- Bank Statutory Tab -->
            <div class="tab-pane fade" id="bank_statutory">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Cuadernos</h3>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Codigo</th>
                                    <th>Venta</th>
                                    <th>Cobranza</th>
                                    <th>Gasto</th>
                                    <th>Garantia</th>
                                    <th>Deuda</th>
                                    <th>Monto Entregado</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user->cuaderno as $cuaderno )
                                <tr>
                                    <td hidden class="id">{{ $cuaderno->id }}</td>
                                    <td>{{ $cuaderno->codigo }}</td>
                                    <td>{{ $cuaderno->total_venta }}</td>
                                    <td>{{ $cuaderno->total_cobranza }}</td>
                                    <td>{{ $cuaderno->total_gasto }}</td>
                                    <td>{{ $cuaderno->total_garantia }}</td>
                                    <td>{{ $cuaderno->total_deuda }}</td>
                                    <td>{{ $cuaderno->total_cuaderno }}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($cuaderno->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        @can('ver-cuaderno')
                                        <a class="btn btn-success btn-sm" href="{{ url('cuadernos/registrar/'.$cuaderno->id) }}"><i class="fa fa-eye"></i></a>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /Bank Statutory Tab -->
        </div>
    </div>
    <!-- /Page Content -->


    <!-- /Page Content -->
</div>
@endsection