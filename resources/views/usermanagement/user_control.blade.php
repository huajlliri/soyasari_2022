@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Usuarios</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Usuarios</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-usuario')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Usuario</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">

                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th hidden>Apellido Paterno</th>
                                    <th hidden>Apellido Materno</th>
                                    <th hidden>Nombre</th>
                                    <th hidden>Nombre</th>

                                    <th>Rol</th>
                                    <th hidden>Carnet</th>
                                    <th>Celular</th>
                                    <th hidden>Direccion</th>
                                    <th>Departamento</th>
                                    <th hidden>Departamento_id</th>
                                    <th>Estado</th>
                                    <th hidden>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user )
                                <tr>

                                    <td>
                                        <span hidden class="image">{{ $user->avatar}}</span>
                                        <h2 class="table-avatar">
                                            <a class="avatar"><img src="{{ URL::to('/assets/images/'. $user->avatar) }}" alt="{{ $user->avatar }}"></a>
                                            <a class="email">{{ $user->email }}</span></a>
                                        </h2>
                                    </td>
                                    <td>{{$user->nombre}} <br> {{$user->apellido_paterno}} {{$user->apellido_materno}}</td>
                                    <td hidden class="ids">{{ $user->id }}</td>
                                    <td hidden class="name">{{ $user->nombre }}</td>
                                    <td hidden class="apellido_paterno">{{ $user->apellido_paterno }}</td>
                                    <td hidden class="apellido_materno">{{ $user->apellido_materno }}</td>
                                    <td>

                                        @foreach($user->getRoleNames() as $rolNombre)
                                        <span class="badge bg-inverse-danger role_name">{{$rolNombre}}</span>
                                        @endforeach

                                    </td>
                                    <td hidden class="carnet">{{ $user->carnet }}</td>
                                    <td class="phone_number">{{ $user->telefono }}</td>
                                    <td hidden class="direccion">{{ $user->direccion }}</td>


                                    <td class="department">{{ $user->departamento->nombre }}</td>
                                    <td hidden class="department_id">{{ $user->departamento->id  }}</td>


                                    <td>
                                        <div class="dropdown action-label">
                                            @if ($user->estado=='Activo')
                                            <a style="color:green">
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span class="statuss">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='Inactivo')
                                            <a style="color:blue">
                                                <i class="fa fa-dot-circle-o text-info"></i>
                                                <span class="statuss text-info">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='Deshabilitado')
                                            <a style="color:red">
                                                <i class="fa fa-dot-circle-o "></i>
                                                <span class="statuss">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='')
                                            <a>
                                                <i class="fa fa-dot-circle-o text-dark"></i>
                                                <span class="statuss text-dark">N/A</span>
                                            </a>
                                            @endif
 

                                        </div>
                                    </td>
                                    <td hidden class="created_at">{{\Carbon\Carbon::parse($user->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        @can('ver-perfil-usuario')
                                        <a class="btn btn-success btn-sm" href="{{ url('usuarios/perfil/'.$user->id) }}"><i class="fa fa-eye"></i></a>
                                        @endcan
                                        @can('editar-usuario')
                                        <a class="btn btn-info btn-sm userUpdate" href="#" data-toggle="modal" data-id="'.$user->id.'" data-target="#edit_user"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('borrar-usuario')
                                        @if($user->id!=Auth::user()->id && $user->cliente=='[]' && $user->cuaderno=='[]' )
                                        <a class="btn btn-danger btn-sm userDelete" href="#" data-toggle="modal" ata-id="'.$user->id.'" data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                        @endif
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('user/add/save') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Codigo de Empleado</label>
                                <input required class="form-control" min="1000" max="9999" type="number" id="email" name="email" placeholder="ej: 8080">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input required class="form-control @error('name') is-invalid @enderror" type="text" id="name" name="name" placeholder="ej: Juan Carlos">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apellido Paterno</label>
                                    <input required class="form-control" type="text" id="apellido_paterno" name="apellido_paterno" placeholder="ej: Avila">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Apellido Materno</label>
                                <input class="form-control" type="text" id="apellido_materno" name="apellido_materno" placeholder="ej: Santander">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <label>Rol</label>
                                <select required class="select" name="role_name" id="role_name">
                                    <option selected disabled value=""> --Seleccione--</option>
                                    @foreach ($roles as $role )
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Carnet</label>
                                <input required class="form-control" type="text" id="carnet" name="carnet" placeholder="ej: 4455667LP">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input required class="form-control" type="tel" id="phone" name="phone" placeholder="ej: 77889977">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Dirección</label>
                                <input required class="form-control" type="text" id="direccion" name="direccion" placeholder="ej: Calle Bolivar">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Departamento</label>
                                <select required class="select" name="department" id="department">
                                    <option selected disabled value=""> --Seleccione--</option>
                                    @foreach ($departamentos as $departamento )
                                    <option value="{{ $departamento->id }}">{{ $departamento->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Estado</label>
                                <select required class="select" name="status" id="status">
                                    <option selected disabled value=""> --Seleccione--</option>
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                    <option value="Deshabilitado">Deshabilitado</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contraseña Numérica</label>
                                    <input required type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control" name="password" placeholder="ej: 12345678">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Confirme Contraseña Numérica</label>
                                <input required type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control" name="password_confirmation" placeholder="ej: 12345678">
                            </div>
                            <div class="col-sm-6">
                                <label>Genero</label><br>
                                <select required class="select" name="genero" id="genero">
                                    <option selected disabled value=""> --Seleccione--</option>
                                    <option value="masculino">Masculino</option>
                                    <option value="femenino">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->

    <!-- Edit User Modal -->
    <div id="edit_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('update') }}" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <input type="hidden" value="" id="e_ids" name="ids">
                        <input type="hidden" name="rec_id" id="e_id" value="">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Codigo de empleado</label>
                                <input disabled required class="form-control" type="number" name="email" id="e_email" value="" />
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input required class="form-control" type="text" name="name" id="e_name" value="" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apellido Paterno</label>
                                    <input required class="form-control" type="text" id="e_apellido_paterno" name="apellido_paterno" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Apellido Materno</label>
                                <input class="form-control" type="text" id="e_apellido_materno" name="apellido_materno" value="" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Rol</label>


                                <select class="select" name="role_name" id="e_role_name">

                                    @foreach ($roles as $role )
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Carnet</label>
                                <input required class="form-control" type="text" id="e_carnet" name="carnet" value="" />
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input required class="form-control" type="text" id="e_phone_number" name="phone">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Dirección</label>
                                <input required class="form-control" type="text" id="e_direccion" name="direccion">
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Departamento</label>
                                <select class="select" name="department" id="e_department">
                                    @foreach ($departamentos as $departamento )
                                    <option value="{{ $departamento->id }}">{{ $departamento->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Estado</label>
                                <select class="select" name="status" id="e_status">
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                    <option value="Deshabilitado">Deshabilitado</option>
                                </select>
                            </div>


                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contraseña Numérica</label>
                                    <input type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control" name="password" placeholder="ej: 12345678">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Confirme Contraseña Numérica</label>
                                <input type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control" name="password_confirmation" placeholder="ej: 12345678">
                            </div>
                            <div class="col-sm-6">
                                <label>Genero</label><br>
                                <select class="select" name="generoe" id="generoe">
                                    <option selected disabled value=""> --Seleccione--</option>
                                    <option value="masculino">Masculino</option>
                                    <option value="femenino">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Usuario</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('user/delete') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" class="e_id" value="">
                            <input type="hidden" name="avatar" class="e_avatar" value="">
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_ids').val(_this.find('.ids').text());
        $('#e_name').val(_this.find('.name').text());
        $('#e_apellido_paterno').val(_this.find('.apellido_paterno').text());
        $('#e_apellido_materno').val(_this.find('.apellido_materno').text());
        $('#e_email').val(_this.find('.email').text());
        $('#e_carnet').val(_this.find('.carnet').text());
        $('#e_phone_number').val(_this.find('.phone_number').text());
        $('#e_direccion').val(_this.find('.direccion').text());
        $('#e_ids').val(_this.find('.ids').text());


        var role_name = (_this.find(".role_name").text());
        var _option1 = '<option selected value="' + role_name + '">' + _this.find('.role_name').text() + '</option>'
        $(_option1).appendTo("#e_role_name");


        var department = (_this.find(".department").text());
        var department_id = (_this.find(".department_id").text());
        var _option2 = '<option selected value="' + department_id + '">' + _this.find('.department').text() + '</option>'
        $(_option2).appendTo("#e_department");

        var statuss = (_this.find(".statuss").text());
        var _option3 = '<option selected value="' + statuss + '">' + _this.find('.statuss').text() + '</option>'
        $(_option3).appendTo("#e_status");

    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.ids').text());
        $('.e_avatar').val(_this.find('.image').text());
    });
</script>


<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>


<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection