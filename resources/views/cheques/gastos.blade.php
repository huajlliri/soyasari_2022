@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Gastos</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Gastos</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->

        <!-- /Search Filter -->
        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <center>
                    <h3>GASTOS MENSUALES DE VENDEDORES</h3>
                </center>
                <div class="row staff-grid-row">
                    @foreach ($vendedores as $vendedor )
                    <?php
                    $gast=0;
                    ?>
                    @foreach($vendedor->cuaderno as $cuaderno)
                    @foreach($cuaderno->gastos_cuaderno_diario as $gastoss)
                    <?php
                    $gast=$gast+$gastoss->monto;
                    ?>
                    @endforeach
                    @endforeach
                    @if($gast>0)
                    <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
                        <div class="profile-widget">
                            <div class="profile-img">
                                <a class="avatar"><img src="{{ URL::to('/assets/images/'. $vendedor->avatar) }}"></a>
                            </div>
                            <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a>Bs. {{ $gast}}</a></h4>
                            <div class="small text-muted">{{ $vendedor->nombre.' '.$vendedor->apellido_paterno }}</div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Cuaderno</th>
                                    <th>Vendedor</th>
                                    <th>Descripción</th>
                                    <th>Monto</th>
                                    <th>Fecha de Alta</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($gastos as $gasto )
                                <tr>
                                    <td hidden class="id">{{ $gasto->id }}</td>
                                    <td class="codigo">{{ $gasto->cuaderno->codigo }}</td>
                                    <td class="cliente">{{ $gasto->cuaderno->user->nombre.' '.$gasto->cuaderno->user->apellido_paterno}}</td>
                                    <td class="metodo_pago">{{ $gasto->descripcion }}</td>
                                    <td class="total_venta">{{ $gasto->monto }}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($gasto->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

</div>
<!-- /Page Wrapper -->
@section('script')

@endsection

@endsection