@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Pagos por Confirmar</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pagos por Confirmar</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->

        <!-- /Search Filter -->
        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>PAGOS PENDIENTES</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Metodo de Pago</th>
                                    <th>Monto</th>
                                    <th>Estado</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pendientes as $pendiente )
                                <tr>
                                    <td hidden class="id">{{ $pendiente->id }}</td>
                                    <td class="codigo">{{ $pendiente->cuaderno->codigo }}</td>
                                    <td class="cliente">{{ $pendiente->cliente->nombre.' '.$pendiente->cliente->apellido_paterno}}</td>

                                    <td class="metodo_pago">{{ $pendiente->metodo_pago }}</td>
                                    <td class="total_venta">{{ $pendiente->total_venta }}</td>

                                    <td class="metodo_pago_estado text-danger">{{ $pendiente->metodo_pago_estado }}</td>


                                    <td class="created_at">{{\Carbon\Carbon::parse($pendiente->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        @can('verificar-transferencias')
                                        <a class="btn btn-info btn-sm userUpdate" data-toggle="modal" data-id="'.$user->id.'" data-target="#edit_user"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                                @foreach ($pendientes2 as $pendiente )
                                <tr>
                                    <td hidden class="idcuenta">{{ $pendiente->id }}</td>
                                    <td class="codigo">{{ $pendiente->cuaderno->codigo }}</td>
                                    <td class="cliente">{{ $pendiente->cuenta->historial_cuaderno->cliente->nombre.' '.$pendiente->cuenta->historial_cuaderno->cliente->apellido_paterno}}</td>

                                    <td class="metodo_pago">{{ $pendiente->metodo_pago }}</td>
                                    <td class="total_venta">{{ $pendiente->monto }}</td>

                                    <td class="metodo_pago_estado text-danger">{{ $pendiente->metodo_pago_estado }}</td>


                                    <td class="created_at">{{\Carbon\Carbon::parse($pendiente->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        @can('verificar-transferencias')
                                        <a class="btn btn-info btn-sm userUpdate2" data-toggle="modal" data-id="'.$user->id.'" data-target="#edit_user2"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>PAGOS VERIFICADOS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Metodo de Pago</th>
                                    <th>Monto</th>
                                    <th>Estado</th>
                                    <th>Fecha de Alta</th>
                                    <th>Fecha de Validación</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($pagados as $pagado )
                                <tr>
                                    <td hidden class="id">{{ $pagado->id }}</td>
                                    <td class="codigo">{{ $pagado->cuaderno->codigo }}</td>
                                    <td class="cliente">{{ $pagado->cliente->nombre.' '.$pagado->cliente->apellido_paterno}}</td>

                                    <td class="metodo_pago">{{ $pagado->metodo_pago }}</td>
                                    <td class="total_venta">{{ $pagado->total_venta }}</td>

                                    <td class="metodo_pago_estado text-success">{{ $pagado->metodo_pago_estado }}</td>

                                    <td class="created_at">{{\Carbon\Carbon::parse($pagado->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="update_at">{{\Carbon\Carbon::parse($pagado->update_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endforeach

                                @foreach ($pagados2 as $pagado )
                                <tr>
                                    <td hidden class="idcuenta">{{ $pagado->id }}</td>
                                    <td class="codigo">{{ $pagado->cuaderno->codigo }}</td>
                                    <td class="cliente">{{ $pagado->cuenta->historial_cuaderno->cliente->nombre.' '.$pagado->cuenta->historial_cuaderno->cliente->apellido_paterno}}</td>

                                    <td class="metodo_pago">{{ $pagado->metodo_pago }}</td>
                                    <td class="total_venta">{{ $pagado->monto }}</td>

                                    <td class="metodo_pago_estado text-success">{{ $pagado->metodo_pago_estado }}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($pagado->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="update_at">{{\Carbon\Carbon::parse($pagado->update_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Edit User Modal -->
    <div id="edit_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Verificar Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('updatepago') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf

                        <input type="hidden" name="id" id="e_id" value="">
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="select" name="metodo_pago_estado" id="metodo_pago_estado">
                                <option value="Pendiente">Pendiente</option>
                                <option value="Pagado">Verificado</option>
                            </select>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->

    <!-- Edit User Modal -->
    <div id="edit_user2" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Verificar Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('updatepagocuenta') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf

                        <input type="hidden" name="idcuenta" id="e_idcuenta" value="">
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="select" name="metodo_pago_estado2" id="metodo_pago_estado2">
                                <option value="Pendiente">Pendiente</option>
                                <option value="Pagado">Pagado</option>
                            </select>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->




</div>
<!-- /Page Wrapper -->
@section('script')

{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_name').val(_this.find('.name').text());
        $('#e_id').val(_this.find('.id').text());
    });
</script>


{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate2', function() {
        var _this = $(this).parents('tr');
        $('#e_idcuenta').val(_this.find('.idcuenta').text());
    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection