@extends('layouts.master')
@section('content')

<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Productos</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Productos</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-producto')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Producto</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->



        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">

                            <thead>
                                <tr>
                                    <th>id</th>

                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Precio</th>
                                    <th>Capacidad</th>
                                    <th>Estado</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productos as $producto )
                                <tr>
                                    <td>
                                        <span hidden class="image">{{ $producto->avatar}}</span>
                                        <h2 class="table-avatar">
                                            <a class="avatar"><img src="{{ URL::to('/assets/images/productos/'. $producto->avatar) }}" height="40px" width="40px"></a>
                                            <a class="id">{{ $producto->id }}</span></a>
                                        </h2>
                                    </td>

                                    <td class="nombre">{{$producto->nombre}}</td>
                                    <td class="descripcion">{{$producto->descripcion}}</td>
                                    <td class="precio">{{$producto->precio}}</td>
                                    <td class="tipo">{{$producto->capacidad}}</td>
                                    <td class="estado">{{$producto->estado}}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($producto->created_at))}}</td>

                                    <td class="text-right">
                                        @can('editar-producto')
                                        <a class="btn btn-info btn-sm userUpdate" data-toggle="modal" data-id="'.$cliente->id.'" data-target="#edit_user"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('borrar-producto')
                                        @if($producto->historial_cuaderno=='[]')
                                        <a class="btn btn-danger btn-sm userDelete" href="#" data-toggle="modal" ata-id="'.$cliente->id.'" data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                        @endif
                                        @endcan

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('producto/add/save') }}" method="POST" enctype="multipart/form-data" id="formulario" onsubmit="bloquear()">
                        @csrf

                        <div class="row">
                            <div class="col-sm-4">
                                <label>Nombre
                                    <span style="color:red;">*</span>
                                </label>
                                <input required class="form-control" type="text" id="nombre" name="nombre" placeholder="ej: Turril">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Descripción
                                        <span style="color:red;">*</span>
                                    </label>
                                    <input required class="form-control" type="text" id="descripcion" name="descripcion" placeholder="ej: Contenido">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input required class="form-control" type="number" id="precio" name="precio" placeholder="ej: 2150">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Capacidad</label>
                                    <input required class="form-control" type="text" id="tipo" name="tipo" placeholder="ej: unico">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input required class="form-control" type="text" id="estado" name="estado" placeholder="ej: elegible">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Foto
                                        <span style="color:red;">*</span>
                                    </label>
                                    <input required class="form-control" type="file" accept="image/*" id="image" name="image">
                                </div>
                            </div>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->



    <!-- Edit User Modal -->
    <div id="edit_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('updateproducto') }}" method="POST" enctype="multipart/form-data" id="formulario2" onsubmit="bloquear2()">
                        @csrf

                        <div class="row">
                            <div class="col-sm-4">
                                <input type="hidden" name="id" id="e_id" value="">

                                <label>Nombre
                                    <span style="color:red;">*</span>
                                </label>
                                <input required class="form-control" type="text" id="e_nombre" name="nombre" placeholder="ej: Turril">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Descripción
                                        <span style="color:red;">*</span>
                                    </label>
                                    <input required class="form-control" type="text" id="e_descripcion" name="descripcion" placeholder="ej: Contenido">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input required class="form-control" type="number" id="e_precio" name="precio" placeholder="ej: 2150">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Capacidad</label>
                                    <input required class="form-control" type="text" id="e_tipo" name="tipo" placeholder="ej: unico">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input required class="form-control" type="text" id="e_estado" name="estado" placeholder="ej: elegible">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Foto
                                        <span style="color:red;">*</span>
                                    </label>
                                    <input class="form-control" type="file" accept="image/*" id="image" name="images">
                                    <input type="hidden" name="hidden_image" id="e_image" value="">

                                </div>
                            </div>
                        </div>


                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Salary Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Producto</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('producto/delete') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" class="e_id" value="">
                            <input type="hidden" name="avatar" class="e_avatar" value="">
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_nombre').val(_this.find('.nombre').text());
        $('#e_descripcion').val(_this.find('.descripcion').text());
        $('#e_precio').val(_this.find('.precio').text());
        $('#e_tipo').val(_this.find('.tipo').text());
        $('#e_estado').val(_this.find('.estado').text());

        $('#e_image').val(_this.find('.image').text());




        var tipo_negocio = (_this.find(".tipo_negocio").text());
        var _option2 = '<option selected value="' + tipo_negocio + '">' + _this.find('.tipo_negocio').text() + '</option>'
        $(_option2).appendTo("#e_tipo_negocio");

        var statuss = (_this.find(".statuss").text());
        var _option3 = '<option selected value="' + statuss + '">' + _this.find('.statuss').text() + '</option>'
        $(_option3).appendTo("#e_status");

    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());
        $('.e_avatar').val(_this.find('.image').text());

    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection