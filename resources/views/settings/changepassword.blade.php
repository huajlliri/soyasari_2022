@extends('layouts.master')
@section('content')


<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <!-- Page Header -->
                <div class="card">
                    <div class="card-body">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="page-title">Cambiar Contraseña</h3>
                                </div>
                            </div>
                        </div>
                        <!-- /Page Header -->
                        <form method="POST" action="{{ route('change/password/db') }}">
                            @csrf
                            <div class="form-group">
                                <label>Contraseña Actual</label>
                                <input type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control @error('current_password') is-invalid @enderror " name="current_password" value="{{ old('current_password') }}">
                                @error('current_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Nueva Contraseña Numérica</label>
                                <input type="password" pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control @error('new_password') is-invalid @enderror" name="new_password" >
                                @error('new_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Confirme la Contraseña Numérica</label>
                                <input type="password"  pattern="[0-9]*" inputmode="numeric" minlength="4" maxlength="8" class="form-control @error('new_confirm_password') is-invalid @enderror" name="new_confirm_password" >
                                @error('new_confirm_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="submit-section">
                                <button type="submit" class="btn btn-primary submit-btn">Actualizar Contraseña</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /Page Content -->
        </div>
    </div>
</div>
<!-- /Page Wrapper -->
@endsection