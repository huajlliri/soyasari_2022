<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Carbon\Carbon;
use Session;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
            'locked',
            'unlock'
        ]);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $email    = $request->email;
        $password = $request->password;



        if (Auth::attempt(['email' => $email, 'password' => $password, 'estado' => 'Activo'])) {
            $user_name = DB::table('users')->where('email', $email)->first()->nombre;
            $user_ap = DB::table('users')->where('email', $email)->first()->apellido_paterno;

            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;

            $activityLog = [

                'nombre'        => $user_name . ' ' . $user_ap,
                'codigo'       => $email,
                'descripcion' => 'Inicio de Sesión',
                'date_time'   => $todayDate,
            ];
            DB::table('registro_sesiones')->insert($activityLog);

            Toastr::success('Sesión Iniciada', 'Listo');
            return redirect()->intended('home');
        
        } else {
            Toastr::error('Acceso Denegado', 'Error');
            return redirect('login');
        }
    }

    public function logout()
    {
        $user = Auth::User();
        Session::put('user', $user);
        $user = Session::get('user');

        $name       = $user->nombre . ' ' . $user->apellido_paterno;
        $email      = $user->email;

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;

        $activityLog = [

            'nombre'        => $name,
            'codigo'       => $email,
            'descripcion' => 'Cierre de Sesión',
            'date_time'   => $todayDate,
        ];
        DB::table('registro_sesiones')->insert($activityLog);
        Auth::logout();
        Toastr::success('Sesión Cerrada', 'Listo');
        return redirect('login');
    }
}
