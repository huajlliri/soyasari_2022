<?php

namespace App\Http\Controllers;

use App\Models\Almacen;
use App\Models\Cuaderno;
use App\Models\Historial_almacen;
use App\Models\Historial_cuadeno_almacen;
use App\Models\Historial_cuenta;
use App\Models\Producto;
use App\Models\Proveedor;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Registro_cambio;
use App\Models\Transportista;

class AlmacenController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-almacen|crear-stok|editar-stok|borrar-stok|borrar-registro-stok', ['only' => ['index']]);
        $this->middleware('permission:crear-stok', ['only' => ['addNewAlmacenSave']]);
        $this->middleware('permission:editar-stok', ['only' => ['registrarAlmacen', 'addNewHistorialalmacenSave']]);
        $this->middleware('permission:borrar-stok', ['only' => ['delete']]);
        $this->middleware('permission:borrar-registro-stok', ['only' => ['deletehis']]);
    }

    public function index()
    {
        $productos = Producto::all();
        $almacenes = Almacen::all();
        return view('almacens.almacens', compact('productos', 'almacenes'));
    }

    public function addNewAlmacenSave(Request $request)
    {
        $almacen = new Almacen();
        $almacen->producto_id = $request->producto_id;
        $almacen->estado = $request->estado;
        $almacen->cantidad_anterior = 0;
        $almacen->cantidad_actual = $request->cantidad_actual;
        $almacen->save();

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes';
        $actividad->registro = $almacen->producto->nombre . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Creado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Toastr::success('Stok Creado', 'Listo');
        return redirect()->route('almacenes.index');
    }


    public function registrarAlmacen($id)
    {

        $historial_almacenes = Historial_almacen::orderBy('id', 'desc')->where('almacen_id', $id)->get();
        $almacen = Almacen::all()->where('id', $id)->first();
        $usuarios = User::all();
        $transportistas = Transportista::all();
        return view('almacens.historial', compact('historial_almacenes', 'almacen', 'usuarios', 'transportistas'));
    }


    public function addNewHistorialalmacenSave(Request $request)
    {
        $almacen_id = $request->almacen_id;
        $historial_almacen = new Historial_almacen();
        $historial_almacen->almacen_id = $almacen_id;
        $historial_almacen->tipo = $request->tipo;
        $historial_almacen->user_id = $request->user_id;
        $historial_almacen->transportista_id = $request->transportista_id;
        $historial_almacen->codigo = $request->codigo;
        $historial_almacen->cantidad = $request->cantidad;
        $historial_almacen->obs = $request->obs;
        $almacen = Almacen::find($almacen_id);
        if ($request->tipo == 'Salida Ventas' || $request->tipo == 'Salida de Envase') {
            $historial_almacen->saldo = $almacen->cantidad_actual - $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual - $request->cantidad;
        } else {
            $historial_almacen->saldo = $almacen->cantidad_actual + $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual + $request->cantidad;
        }
        $almacen->save();
        $historial_almacen->save();


        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes';
        $actividad->registro = $request->tipo . ', ' . $request->cantidad . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Actualizado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Toastr::success('Registro Creado', 'Listo');
        return redirect('almacenes/registrar/' . $almacen_id);
    }



    public function deletehis(Request $request)
    {
        $almacen = Almacen::find($request->almacen_id);
        $historial_almacen = Historial_almacen::find($request->id);

        if ($historial_almacen->tipo == 'Salida Ventas' || $historial_almacen->tipo == 'Salida de Envase') {
            $almacen->cantidad_actual = $almacen->cantidad_actual + $historial_almacen->cantidad;
        } else {
            $almacen->cantidad_actual = $almacen->cantidad_actual - $historial_almacen->cantidad;
        }
        $almacen->save();

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes';
        $actividad->registro = $historial_almacen->tipo . ', ' . $historial_almacen->cantidad . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Historial_almacen::destroy(($request->id));
        Toastr::success('Movimiento Eliminado', 'Listo');
        return redirect('almacenes/registrar/' . $almacen->id);
    }

    public function delete(Request $request)
    {

        $almacen = Almacen::find($request->id);

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes';
        $actividad->registro = $almacen->producto->nombre . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Almacen::destroy(($request->id));
        Toastr::success('Almacen Eliminado', 'Listo');
        return redirect()->route('almacenes.index');
    }


    public function transportistas()
    {
        $transportistas = Transportista::all();
        return view('almacens.transportistas', compact('transportistas'));
    }
    public function addNewTransSave(Request $request)
    {
        try {
            $trans = new Transportista;

            $trans->nombre = $request->nombre;
            $trans->apellido_paterno = $request->apellido_paterno;
            $trans->apellido_materno = $request->apellido_materno;
            $trans->telefono = $request->telefono;
            $trans->save();


            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Transportista';
            $actividad->registro = $request->nombre . ' ' . $request->apellido_paterno;
            $actividad->accion = 'Creado';
            $actividad->date_time = $todayDate;
            $actividad->save();



            Toastr::success('Transportista Creado', 'Listo');
            return redirect()->route('transportistas');
        } catch (\Throwable $th) {
            Toastr::error('Error por favor revise', 'Error');
            return redirect()->route('transportistas');
        }
    }

    // update
    public function updatetrans(Request $request)
    {
        try {
            $id = $request->id;
            $nombre = $request->nombre;
            $apellido_paterno = $request->apellido_paterno;
            $apellido_materno = $request->apellido_materno;
            $telefono = $request->telefono;


            $update = [
                'nombre' => $nombre,
                'apellido_paterno' => $apellido_paterno,
                'apellido_materno' => $apellido_materno,
                'telefono' => $telefono,
            ];
            Transportista::where('id', $id)->update($update);


            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Transportista';
            $actividad->registro = $request->nombre . ' ' . $request->apellido_paterno;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();

            Toastr::success('Transportista Actualizado', 'Listo');
            return redirect()->route('transportistas');
        } catch (\Throwable $th) {
            Toastr::error('Error por favor revise', 'Error');
            return redirect()->route('transportistas');
        }
    }
    // delete
    public function deletetrans(Request $request)
    {

        $trans = Transportista::find($request->id);

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Transportista';
        $actividad->registro = $trans->nombre . ' ' . $trans->apellido_paterno;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Transportista::destroy($request->id);
        Toastr::success('Transportista Eliminado', 'Listo');
        return redirect()->route('transportistas');
    }
}
