<?php

namespace App\Http\Controllers;

use App\Models\Historial_cuaderno;
use App\Models\Historial_cuenta;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Registro_cambio;
use Illuminate\Support\Facades\Auth;

class ChequesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-transferencias|verificar-transferencias', ['only' => ['index']]);
        $this->middleware('permission:verificar-transferencias', ['only' => ['updatepago','updatepagocuenta']]);
    }
    public function index()
    {
        $pendientes = Historial_cuaderno::where('metodo_pago', 'Transferencia')->get()->where('metodo_pago_estado', 'pendiente'); 
        $pagados = Historial_cuaderno::where('metodo_pago', 'Transferencia')->get()->where('metodo_pago_estado', 'Pagado');

        $pendientes2 = Historial_cuenta::where('metodo_pago', 'Transferencia')->get()->where('metodo_pago_estado', 'pendiente'); 
        $pagados2 = Historial_cuenta::where('metodo_pago', 'Transferencia')->get()->where('metodo_pago_estado', 'Pagado');



        return view('cheques.cheques', compact('pendientes', 'pagados','pendientes2', 'pagados2'));
    }
    public function updatepago(Request $request)
    {
        $metodo_pago_estado = $request->metodo_pago_estado;
        $id = $request->id;
        $update = [
            'metodo_pago_estado' => $metodo_pago_estado,
        ];
        Historial_cuaderno::where('id', $id)->update($update);

        $his=Historial_cuaderno::find($id);
        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Transferencias';
        $actividad->registro = $his->total_cobrado;
        $actividad->accion = 'Verificado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Toastr::success('Pago Actualizado', 'Listo');
        return redirect()->route('transferencias.index');
    }

    public function updatepagocuenta(Request $request)
    {
        $metodo_pago_estado = $request->metodo_pago_estado2;
        $id = $request->idcuenta;
        $update = [
            'metodo_pago_estado' => $metodo_pago_estado,
        ];
        Historial_cuenta::where('id', $id)->update($update);

        $his=Historial_cuenta::find($id);
        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Transferencias';
        $actividad->registro = $his->monto;
        $actividad->accion = 'Verificado';
        $actividad->date_time = $todayDate;
        $actividad->save();
        Toastr::success('Pago Actualizado', 'Listo');
        return redirect()->route('cheques.index');
    }
}
