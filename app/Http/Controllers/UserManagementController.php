<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Cuenta;
use App\Models\Registro_cambio;
use App\Rules\MatchOldPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserManagementController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-usuario|crear-usuario|editar-usuario|borrar-usuario', ['only' => ['index']]);
        $this->middleware('permission:crear-usuario', ['only' => ['addNewUserSave']]);
        $this->middleware('permission:editar-usuario', ['only' => ['update']]);
        $this->middleware('permission:borrar-usuario', ['only' => ['delete']]);
        $this->middleware('permission:ver-registro', ['only' => ['activityLogInLogOut', 'activityLog']]);
        $this->middleware('permission:ver-perfil-usuario', ['only' => ['ver_usuario']]);
    }
    public function ver_usuario($id)
    {
        $user = User::find($id);
        $cuentas = Cuenta::all();
        return view('usermanagement.perfil', compact('user','cuentas'));
    }
    public function ver_vendedor($id)
    {
        $user = User::find($id);
        $cuentas = Cuenta::all();
        return view('usermanagement.perfil_vendedor', compact('user','cuentas'));
    }
    public function index()
    {
        $users      =  User::all()->whereNotIn('departamento_id',[3]);
        $roles      = Role::all();
        $clientes   = Cliente::all();
        $departamentos  = DB::table('departamentos')->get();
        return view('usermanagement.user_control', compact('clientes', 'roles', 'users', 'departamentos'));
    }
    public function vendedores()
    {
        $users      =  User::all()->where('departamento_id','2');
        $roles      = Role::all();
        $clientes   = Cliente::all();
        $departamentos  = DB::table('departamentos')->get();
        return view('usermanagement.vendedores', compact('clientes', 'roles', 'users', 'departamentos'));
    }
    // search user
    public function searchUser(Request $request)
    {
        
            $users      = DB::table('users')->get();
            $result     = DB::table('users')->get();
            $role_name  = DB::table('role_type_users')->get();
            $position   = DB::table('position_types')->get();
            $department = DB::table('departments')->get();
            $status_user = DB::table('user_types')->get();

            // search by name
            if ($request->name) {
                $result = User::where('name', 'LIKE', '%' . $request->name . '%')->get();
            }

            // search by role name
            if ($request->role_name) {
                $result = User::where('role_name', 'LIKE', '%' . $request->role_name . '%')->get();
            }

            // search by status
            if ($request->status) {
                $result = User::where('status', 'LIKE', '%' . $request->status . '%')->get();
            }

            // search by name and role name
            if ($request->name && $request->role_name) {
                $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                    ->where('role_name', 'LIKE', '%' . $request->role_name . '%')
                    ->get();
            }

            // search by role name and status
            if ($request->role_name && $request->status) {
                $result = User::where('role_name', 'LIKE', '%' . $request->role_name . '%')
                    ->where('status', 'LIKE', '%' . $request->status . '%')
                    ->get();
            }

            // search by name and status
            if ($request->name && $request->status) {
                $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                    ->where('status', 'LIKE', '%' . $request->status . '%')
                    ->get();
            }

            // search by name and role name and status
            if ($request->name && $request->role_name && $request->status) {
                $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                    ->where('role_name', 'LIKE', '%' . $request->role_name . '%')
                    ->where('status', 'LIKE', '%' . $request->status . '%')
                    ->get();
            }

            return view('usermanagement.user_control', compact('users', 'role_name', 'position', 'department', 'status_user', 'result'));
        
    }


    public function activityLog()
    {
        $activityLog = DB::table('registro_cambios')->get();
        return view('usermanagement.registro_cambios', compact('activityLog'));
    }
    // activity log
    public function activityLogInLogOut()
    {
        $activityLog = DB::table('registro_sesiones')->get();
        return view('usermanagement.registro_sesiones', compact('activityLog'));
    }




    // save new user
    public function addNewUserSave(Request $request)
    {
        try {
            $request->validate([
                'email'     => 'required|string|max:255|unique:users',
                'password'  => 'required|string|min:4|confirmed',
                'password_confirmation' => 'required',
            ]);

            

            $user = new User;
            $user->email = request()->email;
            $user->nombre = request()->name;
            $user->apellido_paterno = request()->apellido_paterno;
            $user->apellido_materno = request()->apellido_materno;
            $user->carnet = request()->carnet;
            $user->telefono = request()->phone;
            $user->direccion = request()->direccion;
            $user->departamento_id = request()->department;
            $user->estado  = request()->status;
            $genero = request()->genero;
            if ($genero == "masculino") {
                $user->avatar = "chico.png";
            } elseif ($genero == "femenino") {
                $user->avatar = "chica.png";
            }
            $user->password = request()->password;
            $user['password'] = Hash::make($user['password']);
            $user->save();
            $user->assignRole($request->input('role_name'));


            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Usuarios';
            $actividad->registro = request()->name . ' ' . request()->apellido_paterno;
            $actividad->accion = 'Creado';
            $actividad->date_time = $todayDate;
            $actividad->save();

            Toastr::success('Usuario Creado', 'Listo');
            return redirect()->route('usuarios');
        } catch (\Throwable $th) {
            Toastr::error('Revise los campos', 'Error');
            return redirect()->route('usuarios');
        }
    }

    // update
    public function update(Request $request)
    {
        try {
            if ($request->password != '') {
                if ($request->password == $request->password_confirmation) {
                    $user = User::find($request->ids);
                    $user->update(['password' => Hash::make($request->password)]);
                }
                else{
                    $user = User::find($request->id);
                    $user->update(['password' => Hash::make($request->password)]);
                }
            }
            $id       = $request->ids;
            $nombre         = $request->name;
            $apellido_paterno     = $request->apellido_paterno;
            $apellido_materno     = $request->apellido_materno;
            $carnet         = $request->carnet;
            $phone        = $request->phone;
            $direccion        = $request->direccion;
            $departamento_id   = $request->department;
            $estado       = $request->status;


            if ($request->generoe != '') {
                $genero = request()->generoe;
                if ($genero == "masculino") {
                    $avatar = "chico.png";
                } elseif ($genero == "femenino") {
                    $avatar = "chica.png";
                }
                $update = [

                    'id'       => $id,
                    'nombre'         => $nombre,
                    'apellido_paterno' => $apellido_paterno,
                    'apellido_materno' => $apellido_materno,
                    'carnet' => $carnet,
                    'direccion' => $direccion,
                    'telefono' => $phone,
                    'departamento_id'   => $departamento_id,
                    'estado'       => $estado,
                    'avatar'        => $avatar,
                ];
            } else


            $update = [

                'id'       => $id,
                'nombre'         => $nombre,
                'apellido_paterno' => $apellido_paterno,
                'apellido_materno' => $apellido_materno,
                'carnet' => $carnet,
                'direccion' => $direccion,
                'telefono' => $phone,
                'departamento_id'   => $departamento_id,
                'estado'       => $estado,

            ];

            



            $user = User::find($request->ids);
            DB::table('model_has_roles')->where('model_id', $request->ids)->delete();
            $user->assignRole($request->input('role_name'));



            User::where('id', $request->ids)->update($update);
            DB::commit();

            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Usuarios';
            $actividad->registro = request()->name . ' ' . request()->apellido_paterno;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();
            
            Toastr::success('Usuario Actualizado', 'Listo');
            return redirect()->route('usuarios');
        } catch (\Throwable $th) {
            Toastr::error('La contraseña y el codigo de empleado', 'Revise');
            return redirect()->route('usuarios');
        }
    }
    // delete
    public function delete(Request $request)
    {
        $us = User::find($request->id);
        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Usuarios';
        $actividad->registro = $us->nombre . ' ' . $us->apellido_paterno;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();
        
        User::destroy($request->id);
        Toastr::success('Usuario Eliminado', 'Listo');
        return redirect()->route('usuarios');
    }

    // view change password
    public function changePasswordView()
    {
        return view('settings.changepassword');
    }

    // change password in db
    public function changePasswordDB(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
        DB::commit();
        Toastr::success('Contraseña Actualizada', 'Listo');
        return redirect()->intended('home');
    }
}
