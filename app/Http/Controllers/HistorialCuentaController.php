<?php

namespace App\Http\Controllers;

use App\Models\Cuaderno;
use App\Models\Cuenta;
use App\Models\Historial_cuenta;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Models\Registro_cambio;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HistorialCuentaController extends Controller
{


    public function addNewhisCuentaSave(Request $request)
    {
        $cua_id = $request->cuaderno_id;
        $cuader = Cuaderno::find($cua_id);
        $cuader->total_cobranza = $cuader->total_cobranza + $request->monto;

        $historial = new Historial_cuenta();
        $historial->cuaderno_id = $request->cuaderno_id;
        $historial->cuenta_id = $request->cuenta;
        $historial->monto = $request->monto;

        $historial->metodo_pago = $request->metodo_pago_c;
        if (request()->metodo_pago_c != 'Efectivo') {
            $historial->metodo_pago_estado = 'pendiente';
            $cuader->total_pagos_confirmar = $cuader->total_pagos_confirmar + $request->monto;
        } else {
            $cuader->total_cuaderno = $cuader->total_cuaderno + $request->monto;
            $historial->metodo_pago_estado = 'Pagado';
        }


        $historial->obs = $request->obs_c;


        $cuenta = Cuenta::find($request->cuenta);
        $cuenta->saldo = $cuenta->saldo - $request->monto;
        if ($cuenta->saldo > 0)
            $cuenta->estado = 'pendiente';
        else
            $cuenta->estado = 'completado';

        $historial->saldo = $cuenta->saldo;
        $historial->save();
        $cuenta->save();
        $cuader->save();



        Toastr::success('Registro Creado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }

    public function deletehiscuenta(Request $request)
    {
        $cuenta = Cuenta::find($request->cuenta_id);
        $cuaderno = Cuaderno::find($request->cuaderno_id);
        $hiscuenta = Historial_cuenta::find($request->hiscuenta_id);

        $cuenta->saldo = $cuenta->saldo + $hiscuenta->monto;
        if ($cuenta->saldo > 0)
            $cuenta->estado = 'pendiente';
        else
            $cuenta->estado = 'completado';
        $cuenta->save();

        $cuaderno->total_cobranza = $cuaderno->total_cobranza - $hiscuenta->monto;
        if ($hiscuenta->metodo_pago != 'Efectivo') {
            $cuaderno->total_pagos_confirmar = $cuaderno->total_pagos_confirmar - $hiscuenta->monto;
        } else {
            $cuaderno->total_cuaderno = $cuaderno->total_cuaderno - $hiscuenta->monto;
        }
        $cuaderno->save();

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Cobranza';
        $actividad->registro=$hiscuenta->cuaderno->codigo.', '.$hiscuenta->cuenta->historial_cuaderno->cliente->nombre.' '.$hiscuenta->cuenta->historial_cuaderno->cliente->apellido_paterno.', '.$hiscuenta->monto;
        $actividad->accion='Eliminado';
        $actividad->date_time=$todayDate;
        $actividad->save();


        Historial_cuenta::destroy($request->hiscuenta_id);

        Toastr::success('Cobro Eliminado', 'Listo');

        return redirect('cuadernos/registrar/' . $request->cuaderno_id);
    }
}
