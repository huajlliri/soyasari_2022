<?php

namespace App\Http\Controllers;
use App\Models\Producto;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Registro_cambio;

class ProductoController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-producto|crear-producto|editar-producto|borrar-producto', ['only' => ['index']]);
        $this->middleware('permission:crear-producto', ['only' => ['addNewProductoSave']]);
        $this->middleware('permission:editar-producto', ['only' => ['updateproducto']]);
        $this->middleware('permission:borrar-producto', ['only' => ['deleteproducto']]);
    }

    public function index()
    {
        $productos      = Producto::all();
        return view('productos.productos', compact('productos'));
    }

    // save new producto
    public function addNewProductoSave(Request $request)
    {
        
        $producto = new Producto;
        $producto->nombre=request()->nombre;
        $producto->descripcion=request()->descripcion;
        $producto->precio=request()->precio;
        $producto->capacidad=request()->tipo;
        $producto->estado=request()->estado;
 
        $foto=time().'.'.request()->image->extension(); 
        $producto->avatar=$foto;
        $request->image->move(public_path('assets/images/productos'),$foto);

        $producto->save();


        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Productos';
        $actividad->registro=request()->nombre.' '.request()->descripcion.' '.request()->capacidad.' '.request()->precio;
        $actividad->accion='Creado';
        $actividad->date_time=$todayDate;
        $actividad->save();


        Toastr::success('Producto Creado', 'Listo');
        return redirect()->route('productos.index');
    }

    // update
    public function updateproducto(Request $request)
    {

        $nombre = $request->nombre;
        $descripcion = $request->descripcion;
        $precio = $request->precio;
        $capacidad = $request->tipo;
        $estado = $request->estado;


        $image_name=$request->hidden_image;
        if(!empty($request->file('images'))) {
            unlink('assets/images/productos/' . $request->hidden_image);
            $request->images->move(public_path('assets/images/productos'),$image_name);
        }

        $update = [
            'nombre'        => $nombre,
            'descripcion'   => $descripcion,
            'precio'        => $precio,
            'capacidad'          => $capacidad,
            'estado'        => $estado,
            'avatar'        => $image_name,
            

        ];

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Productos';
        $actividad->registro=$nombre.' '.$descripcion.' '.$capacidad.' '.$precio;
        $actividad->accion='Actualizado';
        $actividad->date_time=$todayDate;
        $actividad->save();


        Producto::where('id', $request->id)->update($update);



        Toastr::success('Producto Actualizado', 'Listo');
        return redirect()->route('productos.index');
    }
    // delete
    public function deleteproducto(Request $request)
    {

        $producto = Producto::find($request->id);

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Productos';
        $actividad->registro=$producto->nombre.' '.$producto->descripcion.' '.$producto->precio;
        $actividad->accion='Eliminado';
        $actividad->date_time=$todayDate;
        $actividad->save();

            unlink('assets/images/productos/' . $request->avatar);
            Producto::destroy($request->id);
            
        Toastr::success('Producto Eliminado', 'Listo');
        return redirect()->route('productos.index');
    }
}
