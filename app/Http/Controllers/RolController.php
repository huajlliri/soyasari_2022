<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use App\Models\Registro_cambio;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RolController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:ver-rol|crear-rol|editar-rol|borrar-rol', ['only' => ['index']]);
         $this->middleware('permission:crear-rol', ['only' => ['addNewRolSave']]);
         $this->middleware('permission:editar-rol', ['only' => ['edit','update']]);
         $this->middleware('permission:borrar-rol', ['only' => ['deleterol']]);
    }
    public function index(Request $request)
    {
         $permission = Permission::all();
         $roles = Role::all();
         $user = User::all();
         $rolePermissions = DB::table("role_has_permissions")
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
         return view('roles.roles',compact('roles','permission','rolePermissions','user'));
    }
    public function addNewRolSave(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|unique:roles,name',
                'permission' => 'required',
            ]);
            $role = Role::create(['name' => $request->input('name')]);
            $role->syncPermissions($request->input('permission'));

            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Roles';
            $actividad->registro = $request->name;
            $actividad->accion = 'Creado';
            $actividad->date_time = $todayDate;
            $actividad->save();





            Toastr::success('Rol Creado','Listo');
            return redirect()->route('roles.index');
        } catch (\Exception $e) {
           
            Toastr::error('El nombre del Rol ya existe o no se le asignó ningun permiso','Error');
            return redirect()->route('roles.index');
        }
    }
    public function deleterol(Request $request)
    {

        $role=DB::table("roles")->where('id',$request->id);
        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Roles';
        $actividad->registro = $role->name;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        DB::table("roles")->where('id',$request->id)->delete();
        Toastr::success('Rol Eliminado','Listo');
        return redirect()->route('roles.index');
    }
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
    
        return view('roles.editar',compact('role','permission','rolePermissions'));
    }
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'permission' => 'required',
            ]);
            $role = Role::find($id);
            $role->name = $request->input('name');
            $role->save();
            $role->syncPermissions($request->input('permission'));

            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Roles';
            $actividad->registro = $role->name;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();



            Toastr::success('Rol Actualizado','Listo');
            return redirect()->route('roles.index');
        } catch (\Exception $e) {
            Toastr::error('Verifique los campos','Error');
            return redirect()->route('roles.edit',$id);
        }                  
    }
}