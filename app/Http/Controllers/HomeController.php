<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\User;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $clientes = Cliente::all();
        $usuarios = User::all();
        return view('dashboard.dashboard', compact('clientes','usuarios'));
    }
    public function error_403()
    {
        return view('errors.404');
    }
}
