<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gastos_cuaderno_diario extends Model
{
    use HasFactory;
    protected $fillable = [
        'descripcion',
        'monto',
        'cuaderno_id',
    ];
    public function cuaderno(){
        return $this->hasOne('App\Models\Cuaderno','id','cuaderno_id');
    }
}
