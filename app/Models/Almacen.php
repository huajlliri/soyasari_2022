<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    use HasFactory;
    protected $fillable = [
            'producto_id',
            'estado',
            'cantidad_anterior',
            'cantidad_actual',
    ];

    public function producto(){
        return $this->hasOne('App\Models\Producto','id','producto_id');
    }

    public function historial_almacen(){
        return $this->hasMany('App\Models\Historial_almacen','almacen_id','id'); 
    }


}
