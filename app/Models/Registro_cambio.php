<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registro_cambio extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_name',
        'codigo',
        'tabla',
        'registro',
        'accion',
        'date_time'
    ];
}
