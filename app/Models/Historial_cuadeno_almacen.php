<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_cuadeno_almacen extends Model
{
    use HasFactory;

    protected $fillable = [
        "codigo",
        "user_id",
        "producto_id",
        "saldo_anterior",
        "ingreso",
        "total_turril",
        "venta_turril",
        "saldo_carro",
        "recogido",
        "entregado_deposito",
        "cuaderno_id",
             
    ];

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }
    public function producto(){
        return $this->hasOne('App\Models\Producto','id','producto_id');
    }
    public function cuaderno(){
        return $this->hasOne('App\Models\Cuaderno','id','cuaderno_id');
    }


}
